const WINDOW_LOAD_TIMEOUT = 3000;
const ECOSIA_SEARCH = "https://www.ecosia.org/search?q=";
const SEARCH_ENGINES = [
  "duckduckgo.com/?q=",
  "www.google.com/search",
  "www.google.co.uk/search"
];

const handleBeforeNavigate = ({ url }) => {
  const search = new URL(url).searchParams.get("q");
  console.log(`Search detected: "${search}"`);
  chrome.windows.create(
    { url: `${ECOSIA_SEARCH}${search}`, type: "popup", focused: false },
    handleWindowCreate
  );
};

const handleWindowCreate = ({ id }) => {
  setTimeout(() => chrome.windows.remove(id), WINDOW_LOAD_TIMEOUT);
};

chrome.webNavigation.onBeforeNavigate.addListener(handleBeforeNavigate, {
  url: SEARCH_ENGINES.map(url => ({ urlContains: url }))
});
